package mis.pruebas.productos.servicios;

import mis.pruebas.productos.modelos.Cliente;
import mis.pruebas.productos.modelos.Cuenta;
import mis.pruebas.productos.servicios.repositorios.RepositorioCliente;
import mis.pruebas.productos.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    RepositorioCuenta repositorioCuenta;

    public Cliente obtenerCliente(String nroCliente) {
        final Optional<Cliente> r = this.repositorioCliente.findById(nroCliente);
        return r.isPresent() ? r.get() : null;
    }

    public boolean existe(String nroCliente) {
        return this.repositorioCliente.existsById(nroCliente);
    }

    public Page<Cliente> obtenerClientes(int pagina, int tamanioPagina) {
        return this.repositorioCliente.findAll(PageRequest.of(pagina, tamanioPagina));
    }

    public void agregarCliente(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
    }

    public void reemplazarCliente(String nroCliente, Cliente cliente) {
        cliente.nroCliente = nroCliente;
        this.repositorioCliente.save(cliente);
    }

    @Transactional
    public void eliminarClienteConProductos(String nroCliente) {
        this.repositorioCuenta.borrarCuentasCliente(nroCliente);
        this.repositorioCliente.deleteById(nroCliente);
    }

    public void emparcharCliente(String nroCliente, Cliente cliente) {
        this.repositorioCliente.modificarCliente(nroCliente, cliente);
    }
}
