package mis.pruebas.productos.servicios.repositorios;

import mis.pruebas.productos.modelos.Cliente;

public interface RepositorioClienteExtendido {
    public void modificarCliente(String nroCliente, Cliente cliente);
}
