package mis.pruebas.productos.servicios.repositorios;

import mis.pruebas.productos.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioCliente extends MongoRepository<Cliente, String>,
    RepositorioClienteExtendido {
}
