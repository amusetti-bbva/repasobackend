package mis.pruebas.productos.servicios.repositorios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class RepositorioCuentaExtendidoImpl implements RepositorioCuentaExtendido {

    @Autowired
    MongoTemplate mongoTemplate;

    public void modificarCuenta(String nroCuenta, Double saldo, String estado) {
        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(nroCuenta));

        final Update parche = new Update();
        if(saldo != null) parche.set("saldo", saldo);
        if(estado != null) parche.set("estado", estado);

        this.mongoTemplate.updateFirst(filtro, parche, "cuenta");
    }

    public void borrarCuentasCliente(String nroCliente) {
        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("nroCliente").is(nroCliente));
        this.mongoTemplate.findAllAndRemove(filtro, "cuenta");
    }
}
