package mis.pruebas.productos.servicios.repositorios;

import java.util.List;

public interface RepositorioCuentaExtendido {
    public void modificarCuenta(String nroCuenta, Double saldo, String estado);

    public void borrarCuentasCliente(String nroCliente);
}
