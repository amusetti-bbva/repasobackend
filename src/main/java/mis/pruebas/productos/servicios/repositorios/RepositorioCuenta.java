package mis.pruebas.productos.servicios.repositorios;

import mis.pruebas.productos.modelos.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositorioCuenta extends MongoRepository<Cuenta, String>,
    RepositorioCuentaExtendido {


    @Query(value = "{'nroCliente' : ?0 }")
    public List<Cuenta> buscarCuentasCliente(String nroCliente);
}
