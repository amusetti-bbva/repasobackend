package mis.pruebas.productos.servicios.repositorios;

import mis.pruebas.productos.modelos.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public class RepositorioClienteExtendidoImpl implements RepositorioClienteExtendido {

    @Autowired
    MongoTemplate mongoTemplate;

    public void modificarCliente(String nroCliente, Cliente cliente) {
        final Query filtro = new Query();
        filtro.addCriteria(Criteria.where("_id").is(nroCliente));

        final Update parche = new Update();

        ajustarCampo(parche, "nroDocumento", cliente.nroDocumento);
        ajustarCampo(parche, "tipoDocumento", cliente.tipoDocumento);
        ajustarCampo(parche, "nombreCompleto", cliente.nombreCompleto);
        ajustarCampo(parche, "direccion", cliente.direccion);
        ajustarCampo(parche, "correoElectronico", cliente.correoElectronico);
        ajustarCampo(parche, "telefono", cliente.telefono);

        this.mongoTemplate.updateFirst(filtro, parche, cliente.getClass());
    }

    private void ajustarCampo(Update u, String nombre, String valor) {
        if(valor != null && !valor.trim().isEmpty())
            u.set(nombre, valor.trim());
    }
}
