package mis.pruebas.productos.servicios;

import mis.pruebas.productos.modelos.Cuenta;
import mis.pruebas.productos.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    @Autowired
    RepositorioCuenta repositorioCuenta;

    public boolean existe(String nroCuenta) {
        return this.repositorioCuenta.existsById(nroCuenta);
    }

    public Page<Cuenta> obtenerCuentas(int nroPagina, int tamPagina) {
        return this.repositorioCuenta.findAll(PageRequest.of(nroPagina, tamPagina));
    }

    public List<Cuenta> obtenerCuentasCliente(String nroCliente) {
        return this.repositorioCuenta.buscarCuentasCliente(nroCliente);
    }

    public void agregarCuenta(Cuenta cuenta) {
        this.repositorioCuenta.insert(cuenta);
    }

    public void borrarCuenta(String nroCuenta) {
        this.repositorioCuenta.deleteById(nroCuenta);
    }
    public void modificarCuenta(String nroCuenta, Double saldo, String estado) {
        this.repositorioCuenta.modificarCuenta(nroCuenta, saldo, estado);
    }
}
