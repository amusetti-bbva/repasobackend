package mis.pruebas.productos.servicios;

import mis.pruebas.productos.modelos.Cuenta;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface ServicioCuenta {

    public boolean existe(String nroCuenta);

    public Page<Cuenta> obtenerCuentas(int nroPagina, int tamPagina);

    public List<Cuenta> obtenerCuentasCliente(String nroCliente);

    public void agregarCuenta(Cuenta cuenta);
    public void borrarCuenta(String nroCuenta);
    public void modificarCuenta(String nroCuenta, Double saldo, String estado);
}
