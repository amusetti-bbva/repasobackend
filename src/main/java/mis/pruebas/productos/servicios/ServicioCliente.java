package mis.pruebas.productos.servicios;

import mis.pruebas.productos.modelos.Cliente;
import org.springframework.data.domain.Page;

import java.util.Collections;
import java.util.List;

public interface ServicioCliente {

    public boolean existe(String nroCliente);

    public Cliente obtenerCliente(String nroCliente);

    public Page<Cliente> obtenerClientes(int pagina, int tamanioPagina);

    public void agregarCliente(Cliente cliente);

    public void reemplazarCliente(String nroCliente, Cliente cliente);

    public void eliminarClienteConProductos(String nroCliente);

    public void emparcharCliente(String nroCliente, Cliente cliente);
}
