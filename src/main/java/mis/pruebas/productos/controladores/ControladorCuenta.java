package mis.pruebas.productos.controladores;

import mis.pruebas.productos.modelos.Cuenta;
import mis.pruebas.productos.servicios.ServicioCliente;
import mis.pruebas.productos.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/productos/v1/cuentas")
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    @Autowired
    ServicioCliente servicioCliente;

    // CRUD

    @GetMapping
    public Page<Cuenta> obtenerCuentas(
            @RequestParam(required = false) Integer nroPagina,
            @RequestParam(required = false) Integer tamPagina
    ) {
        // Controlar parámetros paginación.
        if(nroPagina == null || nroPagina < 0)
            nroPagina = 0;
        if(tamPagina == null || tamPagina < 20)
            tamPagina = 20;
        if(tamPagina > 100)
            tamPagina = 100;

        return this.servicioCuenta.obtenerCuentas(nroPagina, tamPagina);
    }

    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta) {

        // VALIDACION DE DATOS
        if(!this.servicioCliente.existe(cuenta.nroCliente)) {
            System.out.println("No existe el cliente: " + cuenta.nroCliente);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if(cuenta.saldo <= 0) {
            System.out.println("Debe inicializar con saldo positivo: " + cuenta.saldo);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        this.servicioCuenta.agregarCuenta(cuenta);
    }

    @DeleteMapping("/{nroCuenta}")
    public void borrarCuenta(@PathVariable String nroCuenta) {
        this.servicioCuenta.borrarCuenta(nroCuenta);
    }

    public static class AjusteCuenta {
        Double saldo;
        String estado;
    }

    @PatchMapping("/{nroCuenta}")
    public void modificarCuenta(@PathVariable String nroCuenta, @RequestBody AjusteCuenta ajusteCuenta) {

        // validar cuenta.
        if(!this.servicioCuenta.existe(nroCuenta))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        // Validamos datos de entrada.
        if(ajusteCuenta.estado != null && !ajusteCuenta.estado.trim().isEmpty()) {
            // activa, bloqueada
            if(!"activa".equalsIgnoreCase(ajusteCuenta.estado.trim())
            && !"bloqueada".equalsIgnoreCase(ajusteCuenta.estado.trim())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
        }

        this.servicioCuenta.modificarCuenta(nroCuenta, ajusteCuenta.saldo, ajusteCuenta.estado);
    }
}
