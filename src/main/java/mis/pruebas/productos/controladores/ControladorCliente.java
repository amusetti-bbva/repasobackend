package mis.pruebas.productos.controladores;

import mis.pruebas.productos.modelos.Cliente;
import mis.pruebas.productos.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/productos/v1/clientes")
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping
    public Page<Cliente> obtenerClientes(
            @RequestParam(required = false) Integer nroPagina,
            @RequestParam(required = false) Integer tamPagina
    ) {
        // Controlar parámetros paginación.
        if(nroPagina == null || nroPagina < 0)
            nroPagina = 0;
        if(tamPagina == null || tamPagina < 20)
            tamPagina = 20;
        if(tamPagina > 100)
            tamPagina = 100;

        return this.servicioCliente.obtenerClientes(nroPagina, tamPagina);
    }

    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {

        this.servicioCliente.agregarCliente(cliente);

        final Link self = linkTo(methodOn(this.getClass()).obtenerCliente(cliente.nroCliente))
                .withSelfRel();

        return ResponseEntity.ok().location(self.toUri()).build();
    }

    @GetMapping("/{nroCliente}")
    public Cliente obtenerCliente(@PathVariable String nroCliente) {
        return this.servicioCliente.obtenerCliente(nroCliente);
    }

    @PutMapping("/{nroCliente}")
    public void reemplazarCliente(@PathVariable String nroCliente, @RequestBody Cliente clienteNuevo) {
        this.servicioCliente.reemplazarCliente(nroCliente, clienteNuevo);
    }

    @DeleteMapping("/{nroCliente}")
    public void eleminarCliente(@PathVariable String nroCliente) {
        this.servicioCliente.eliminarClienteConProductos(nroCliente);
    }

    @PatchMapping("/{nroCliente}")
    public void modificarCliente(@PathVariable String nroCliente, @RequestBody Cliente cliente) {
        this.servicioCliente.emparcharCliente(nroCliente, cliente);
    }

}
