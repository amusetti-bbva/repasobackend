package mis.pruebas.productos.controladores;

import mis.pruebas.productos.modelos.Cuenta;
import mis.pruebas.productos.servicios.ServicioCliente;
import mis.pruebas.productos.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/productos/v1/clientes/{nroCliente}/cuentas")
public class ControladorCuentasCliente {

    @Autowired
    ServicioCuenta servicioCuenta;

    @Autowired
    ServicioCliente servicioCliente;

    // CRUD

    @GetMapping
    public List<Cuenta> obtenerCuentas(@PathVariable String nroCliente) {
        return this.servicioCuenta.obtenerCuentasCliente(nroCliente);
    }

    @PostMapping
    public void agregarCuenta(@PathVariable String nroCliente, @RequestBody Cuenta cuenta) {

        // VALIDACION DE DATOS
        if(!this.servicioCliente.existe(nroCliente)) {
            System.out.println("No existe el cliente: " + nroCliente);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if(cuenta.saldo <= 0) {
            System.out.println("Debe inicializar con saldo positivo: " + cuenta.saldo);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        cuenta.nroCliente = nroCliente;
        this.servicioCuenta.agregarCuenta(cuenta);
    }

    @DeleteMapping("/{nroCuenta}")
    public void borrarCuenta(@PathVariable String nroCliente, @PathVariable String nroCuenta) {
        this.servicioCuenta.borrarCuenta(nroCuenta);
    }

    public static class AjusteCuenta {
        Double saldo;
        String estado;
    }

    @PatchMapping("/{nroCuenta}")
    public void modificarCuenta(@PathVariable String nroCliente, @PathVariable String nroCuenta, @RequestBody ControladorCuenta.AjusteCuenta ajusteCuenta) {

        // validar cuenta.
        if(!this.servicioCuenta.existe(nroCuenta))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        // Validamos datos de entrada.
        if(ajusteCuenta.estado != null && !ajusteCuenta.estado.trim().isEmpty()) {
            // activa, bloqueada
            if(!"activa".equalsIgnoreCase(ajusteCuenta.estado.trim())
                    && !"bloqueada".equalsIgnoreCase(ajusteCuenta.estado.trim())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }
        }

        this.servicioCuenta.modificarCuenta(nroCuenta, ajusteCuenta.saldo, ajusteCuenta.estado);
    }

}
