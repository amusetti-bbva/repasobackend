package mis.pruebas.productos.modelos;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public class Cuenta {
    @Id   @JsonProperty(access= JsonProperty.Access.READ_ONLY)
    public String nroCuenta;

    public String tipoCuenta;
    public String nroCliente;
    public Double saldo;
    public String moneda;
    public String estado;
}
