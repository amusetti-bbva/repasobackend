package mis.pruebas.productos.modelos;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Cliente {

    @Id  @JsonProperty(access= JsonProperty.Access.READ_ONLY)
    public String nroCliente;

    public String nroDocumento;
    public String tipoDocumento;
    public String nombreCompleto;
    public String direccion;
    public String correoElectronico;
    public String telefono;
}
